CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

 
INTRODUCTION
------------

The CSRF Protection module provides form_token for drupal forms to 
Anonymous users.

REQUIREMENTS
------------

No special requirements.

INSTALLATION
------------
 
Install as you would normally install a contributed 
Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 
for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.

MAINTAINERS
-----------

Tharick rick (Tharick) - https://www.drupal.org/user/3103023

<?php

/**
 * @file
 * Provides form_token for all the forms.
 */

/**
 * Implements hook_help().
 */
function csrf_protection_help($path, $arg) {
  switch ($path) {
    case 'admin/config/development/csrf_token':
      return '<p>' . t('Drupal provides CSRF protection for Authenticated user, not Anonymous user. By using this module you can eliminate the CSRF vulnerability from Scanning tools like Acunetix.') . '</p>';

    case 'admin/help#csrf_token':
      $output = '';
      $output .= '<p>' . t('Drupal provides CSRF protection for Authenticated user, not Anonymous user. By using this module you can eliminate the CSRF vulnerability from Scanning tools like Acunetix') . '</p>';
      return $output;
  }
}

/**
 * Generating form token for anonymous users' form.
 */
function csrf_protection_form_alter(&$form, &$form_state, $form_id) {
  if (!isset($form['#token'])) {
    $form['csrf_token']  = array(
      '#type' => 'token',
      '#default_value' => drupal_get_token(),
    );
    $form['#validate'][] = 'csrf_token_validate_anonymous';
    // Store current session ID.
    $session_id = session_id();
    if (isset($_SESSION)) {
      $_SESSION['csrf_session_id'] = $session_id;
    }
    else {
      $_SESSION = array(
        'csrf_session_id' => $session_id,
      );
    }
  }
}

/**
 * Rechecking form token for anonymous users' form.
 */
function csrf_protection_validate_anonymous($form, &$form_state) {
  $token = '';
  if (isset($form_state['values']['csrf_token'])) {
    $token = $form_state['values']['csrf_token'];
    if (!drupal_valid_token($token)) {
      // Not a valid token!
      $path  = current_path();
      $query = drupal_get_query_parameters();
      $url   = url($path, array(
        'query' => $query,
      ));
      // Setting this error will cause the form to fail validation.
      form_set_error('form_token', t('The form has become outdated. Copy any unsaved work in the form below and then <a href="@link">reload this page</a>.', array(
        '@link' => $url,
      )));
    }
  }
}
